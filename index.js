console.warn(`Hello World`);

function numb(){
	let n1 = parseInt(prompt(`Enter a number`));

	if (n1 <= 0){
		alert(`STOOOOP`);
	}
	else{
		// while
		/*while(n1 >= 1){
			console.warn(`Hi user! ${n1}`);

			n1 = n1 - 1;
		}*/

		// for
		for (n1; n1 >= 1; n1--){
			if (n1 <= 50){
				break;
			}
			else{
				console.warn(`Hi user! ${n1}`);
				if(n1 % 10 == 0){
					console.error(`Number ${n1} is being skipped`);
					continue;
				}
				if (n1 % 5 == 0){
					console.log(`Number ${n1} printed`);
				}
			}
		}
	}
}

function str(){
	let str1 = `supercalifragilisticexpialidocious`;
	let str2 = ``;
	let str3 = ``;

	for (let x=0; x < str1.length; x++){
		 console.log(`Word: ${str1.toUpperCase()}`);

		if (str1[x] == `a` ||
			str1[x] == `e` ||
			str1[x] == `i` ||
			str1[x] == `o` ||
			str1[x] == `u`){
			console.warn(`Skip Vowel: ${str1[x]}, index: ${x}`);
			console.log(`Collected vowels: ${str3 = str3 + str1[x]}`);
			continue;
		}

		else{
			str2 = str2 + str1[x];
			console.error(`Consonants: ${str2}`);
		}
	}
}

numb();
str();